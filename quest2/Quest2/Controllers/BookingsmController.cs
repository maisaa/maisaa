﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quest2.Models;

namespace Quest2.Controllers
{
    [EnableCors("AllowAnyOrigin")]
    [Produces("application/json")]
    [Route("api/Bookingsm")]
    public class BookingsmController : Controller
    {
        //private AppDbContext _appDbContext;
        private readonly AppDbContext _context;

        private readonly IBookingsRepository _bookingRepositor;
        private readonly ITimeSlotRepository _timeSlotRepository;
        private readonly ITreatmentRepository _treatmentRepository;


        // Constractor 
        public BookingsmController(IBookingsRepository bookingsRepository, ITimeSlotRepository timeSlotRepository, ITreatmentRepository treatmentRepository, AppDbContext appDbContext)
        {
            _bookingRepositor = bookingsRepository;
            _timeSlotRepository = timeSlotRepository;
            _treatmentRepository = treatmentRepository;
            _context = appDbContext;
        }
        
        //............................................................
        // GET: api/Bookingsm ............get all Bookings
        [HttpGet]
        public IEnumerable<Bookings> GetBookingss()
        {
            //return _context.Bookingss;
            return _bookingRepositor.GetAllBookings();
        }
        //............................................................
        // GET: api/Bookingsm ............get all TimeSlots
        [HttpGet("time")]
        public IEnumerable<LU_TimeSlot> GetTimeSlots()
        {
            //return _context.Bookingss;
            return _timeSlotRepository.GetAllTimeSlots();
        }
        //............................................................
        // GET: api/Bookingsm ............get all Treatments
        [HttpGet("treatment")]
        public IEnumerable<LU_Treatment> GetTreatments()
        {
            //return _context.Bookingss;
            return _treatmentRepository.GetAllTreatments();
        }
        //.............................................................
        // GET: api/Bookingsm/5 ..........get booking by id
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBookings([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookings =  _bookingRepositor.GetBookingsById(id);

            if (bookings == null)
            {
                return NotFound();
            }

            return Ok(bookings);
        }
        //.............................................................
        // GET: api/Bookingsm/treatment/5 ..........get booking by id
        [HttpGet("treatment/{id}")]
        public async Task<IActionResult> GetTreatmentById([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var treatment = _treatmentRepository.GetTreatmentById(id);

            if (treatment == null)
            {
                return NotFound();
            }

            return Ok(treatment);
        }
        //.............................................................
        // GET: api/Bookingsm/timeSlot/5 ..........get booking by id
        [HttpGet("timeSlot/{id}")]
        public async Task<IActionResult> GetTimeSlotById([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var timeSlot = _timeSlotRepository.GetTimeSlotById(id);

            if (timeSlot == null)
            {
                return NotFound();
            }

            return Ok(timeSlot);
        }
        //..............................................................
        // POST: api/Bookingsm ....................creat new booking
        [HttpPost]
        public async Task<IActionResult> PostBookings([FromBody] Bookings bookings)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _bookingRepositor.AddBookings(bookings);
            //await _bookingRepositor.SaveChangesAsync();

            return CreatedAtAction("GetBookings", new { id = bookings.BookingId }, bookings);
        }
        //...................................................................
        // PUT: api/Bookingsm/put/5...........................edit booking(id)
        [HttpPost("put/{id}")]
        public async Task<IActionResult> PutBookings([FromRoute] int Id, [FromBody] Bookings booking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (Id != booking.BookingId)
            {
                return BadRequest();
            }
            try
            {
                _bookingRepositor.UpdateBooking( Id , booking);

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookingsExists(Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }
        
        //...................................................................
        // DELETE: api/Bookingsm/5..........change isDelete to be true
        [HttpPost("delete/{id}")]
        public async Task<IActionResult> DeleteBookings([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var bookings = await _context.Bookingss.SingleOrDefaultAsync(m => m.BookingId == id);

            //if (bookings == null)
            //{
            //    return NotFound();
            //}

            //_context.Bookingss.Remove(bookings);
            //await _context.SaveChangesAsync();

            _bookingRepositor.DeleteBooking(id);

            return Ok();
        }
        //.............................................
        private bool BookingsExists(int id)
        {
            return _context.Bookingss.Any(e => e.BookingId == id);
        }
    }
}