﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quest2.Models;
using Quest2.ViewModels;

namespace Quest2.Controllers
{
    // [Route("api/[Controller]")]
    [EnableCors("AllowAnyOrigin")]
    public class BookingController : Controller
    {
        private AppDbContext _appDbContext;

        private readonly IBookingsRepository _bookingRepositor;
        private readonly ITimeSlotRepository _timeSlotRepository;
        private readonly ITreatmentRepository _treatmentRepository;


        // Constractor 
        public BookingController(IBookingsRepository bookingsRepository, ITimeSlotRepository timeSlotRepository, ITreatmentRepository treatmentRepository, AppDbContext appDbContext)
        {
            _bookingRepositor = bookingsRepository;
            _timeSlotRepository = timeSlotRepository;
            _treatmentRepository = treatmentRepository;
            _appDbContext = appDbContext;
        }
      
        //................................................
        //Get All Bookings controller:{Booking}/action:{List1}
        public IActionResult List1()
        {
            var bookings = _bookingRepositor.GetAllBookings().OrderBy(p => p.Name);

            foreach (var b in bookings)
            {
                b.TimeSlots = _appDbContext.TimeSlots.Single(t => t.TimeSlotId == b.TimeSlotId);
                b.Treatments = _appDbContext.Treatments.Single(t => t.TreatmentId == b.TreatmentId);

            }
            return View(bookings);
        }
        //................................................
        //Get  Bookings By Name 
        public IActionResult Index(string searchString)
        {
            var bookings = _bookingRepositor.GetBookingsByName(searchString);
            foreach (var b in bookings)
            {
                b.TimeSlots = _appDbContext.TimeSlots.Single(t => t.TimeSlotId == b.TimeSlotId);
                b.Treatments = _appDbContext.Treatments.Single(t => t.TreatmentId == b.TreatmentId);
            }
            bookings = bookings.ToList();
            
            return View(bookings);
        }
        //................................................
        //Get  Bookings By  Mobile Number
        public IActionResult Index2(string searchNum)
        {
            var bookings = _bookingRepositor.GetBookingsByNumber(searchNum);
            foreach (var b in bookings)
            {
                b.TimeSlots = _appDbContext.TimeSlots.Single(t => t.TimeSlotId == b.TimeSlotId);
                b.Treatments = _appDbContext.Treatments.Single(t => t.TreatmentId == b.TreatmentId);
            }
            bookings = bookings.ToList();

            return View(bookings);
        }
        //GetBookingsByNameMobile
        //................................................
        //Get  Bookings By Name && Mobile Number
        public IActionResult IndexBoth(string searchString, string searchNum)
        {
            var bookings = _bookingRepositor.GetBookingsByNameMobile(searchString, searchNum);
            foreach (var b in bookings)
            {
                b.TimeSlots = _appDbContext.TimeSlots.Single(t => t.TimeSlotId == b.TimeSlotId);
                b.Treatments = _appDbContext.Treatments.Single(t => t.TreatmentId == b.TreatmentId);
            }
            bookings = bookings.ToList();

            return View(bookings);
        }
        //............................................  
        //Get Details
        public IActionResult Details(int id)
        {
            var booking = _bookingRepositor.GetBookingsById(id);
            if (booking == null)
                return NotFound();
            
            booking.TimeSlots = _appDbContext.TimeSlots.Single(t => t.TimeSlotId == booking.TimeSlotId);
            booking.Treatments = _appDbContext.Treatments.Single(t => t.TreatmentId == booking.TreatmentId);
 
            return View(booking);
        }

        //............................................  
        // Get Create  controller:{Booking}/action:{Create}
        [HttpGet]
        public IActionResult Create()
        {
            List<LU_TimeSlot> li = new List<LU_TimeSlot>();
            List<LU_Treatment> lii = new List<LU_Treatment>();
            li = _appDbContext.TimeSlots.ToList();
            lii = _appDbContext.Treatments.ToList();
            ViewBag.listOfItems = li;
            ViewBag.listOfTreatment = lii;

            return View();
        }
        //............................................  
        // Post Create  controller:{Booking}/action:{Create}
        [HttpPost]
        public IActionResult Create(Bookings booking)
        {
            //if (ModelState.IsValid)
            //{
                _bookingRepositor.AddBookings(booking);
                return RedirectToAction("List1");
            //}
           // return View(booking);
        }
        //............................................    
        //Delete Booking By Id
        public IActionResult Delete(int id)
        {
            _bookingRepositor.DeleteBooking(id);
           
            return RedirectToAction("List1");
        }

        //............................................
        // Get Edit  controller:{Booking}/action:{Edit}
        [HttpGet]
        public IActionResult Edit(int Id)
        {
            Bookings bookings = _bookingRepositor.GetBookingsById(Id);

            List<LU_TimeSlot> li = new List<LU_TimeSlot>();
            List<LU_Treatment> lii = new List<LU_Treatment>();
            li = _appDbContext.TimeSlots.ToList();
            lii = _appDbContext.Treatments.ToList();
            ViewBag.listOfItems = li;
            ViewBag.listOfTreatment = lii;

            return View("Edit" ,bookings);
        }
        //................................................
        // Post Edit  
        [HttpPost]
        public IActionResult Edit( int Id,Bookings booking)
        {
            Console.WriteLine("............Jsone....1.....");
            Console.WriteLine(JsonConvert.SerializeObject(booking));
            // Console.WriteLine(JObject.Parse(booking.Name));
            Console.WriteLine("............Jsone....2.....");
            _bookingRepositor.UpdateBooking(Id,booking);
             
            return RedirectToAction("List1");
        }
    }
}


//................................................
//Get All Bookings 
//public IActionResult List()
//{
//    var bookings = _bookingRepositor.GetAllBookings().OrderBy(p => p.Name);
//    var bookingListViewModel = new BookingListViewModel()
//    {
//        Bookingss = bookings.ToList(),
//    };

//    return View(bookingListViewModel);
//}
//public ActionResult Delete(int? id)
//{
//    if (id != null)
//    {
//        var data = (from Bookings in _appDbContext.Bookingss
//                    where Bookings.BookingId == id
//                    select Bookings).SingleOrDefault();

//        _appDbContext.Remove(data);
//        _appDbContext.SaveChanges();
//    }
//    return RedirectToPage("List");
//}
//.........................................................
//public async Task<IActionResult> Details(int? id)
//{
//    if (id == null)
//    {
//        return NotFound();
//    }

//    var booking = await _appDbContext.Bookingss
//        .Include(b => b.TimeSlots)
//        .SingleOrDefaultAsync(m => m.BookingId == id);

//    if (booking == null)
//    {
//        return NotFound();
//    }

//    return View(booking);
//}
//............................................
//public ViewResult List()
//{
//    BookingListViewModel bookingListViewModel = new BookingListViewModel();
//    bookingListViewModel.Bookingss = _bookingRepositor.Bookings;

//    bookingListViewModel.TreatmentTitle = "treatment 1";
//    bookingListViewModel.TimeSlotTitle = 1;
//    return View(bookingListViewModel);
//}

//..................................................
//// Return All Bookings as array of objects 
//public JsonResult GetAll()
//{
//    var temp = new JsonResult(_bookingRepositor.GetAllBookings().OrderBy(p => p.Name));
//    temp.StatusCode = 200;

//    //foreach (var b in temp)
//    //{
//    //    b.TimeSlots = _appDbContext.TimeSlots.Single(t => t.TimeSlotId == b.TimeSlotId);
//    //    b.Treatments = _appDbContext.Treatments.Single(t => t.TreatmentId == b.TreatmentId);

//    //}
//    return temp;
//}


