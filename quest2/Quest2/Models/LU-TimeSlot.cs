﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
    public class LU_TimeSlot
    {
        public int TimeSlotId { get; set; }
        public int TimeSlotTitle { get; set; }
       
        public virtual ICollection<Bookings> Bookingss { get; set; }
    }
}
