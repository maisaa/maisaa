﻿using Quest2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
    public class BookingsRepository : IBookingsRepository
    {
        private readonly AppDbContext _appDbContext;

        //Constracter
        public BookingsRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        //..................................................
        //Add new Booking
        public void AddBookings(Bookings bookings)
        {
            bookings.IsDeleted = false;

            _appDbContext.Bookingss.Add(bookings);
            _appDbContext.SaveChanges();
        }
        //..................................................
        //Get All Bookings
        public IEnumerable<Bookings> GetAllBookings()
        {
            var bookings = _appDbContext.Bookingss;
            IList<Bookings> list = new List<Bookings>();
            var B = from b in bookings
                    where b.IsDeleted == false
                    select b;

            foreach (Bookings book in B)
            {
                list.Add(book);
            }
            return list;
        }
        //..................................................
        //Get Booking By ID
        public Bookings GetBookingsById(int bookingsId)
        {
            return _appDbContext.Bookingss.FirstOrDefault(p => p.BookingId == bookingsId);
        }
        //..................................................
        //Get Booking By Name
        public IEnumerable<Bookings> GetBookingsByName(string searchString)
        {
             var bookings = _appDbContext.Bookingss;
             IList<Bookings> list = new List<Bookings>();
             var B = from b in bookings
                where b.Name.Contains(searchString) && b.IsDeleted == false
                     select b;

            foreach (Bookings book in B)
            {
                list.Add(book);
            }
            return list;
        }
        //..................................................
        //Get Booking By Mobile Number
        public IEnumerable<Bookings> GetBookingsByNumber(string searchNum)
        {
            var bookings = _appDbContext.Bookingss;
            IList<Bookings> list = new List<Bookings>();
            var B = from b in bookings
                    where b.Mobile.Contains(searchNum) && b.IsDeleted == false
                    select b;

           
            foreach (Bookings book in B)
            {
                list.Add(book);
            }
            return list;
        }
        //..................................................
        //Updat Booking
        public void UpdateBooking(int bookingsId ,Bookings bookings)
        {
            Bookings booking = GetBookingsById(bookingsId);

                booking.BookedDate = bookings.BookedDate;
                booking.Note = bookings.Note;
                booking.TimeSlotId = bookings.TimeSlotId;
                booking.TreatmentId = bookings.TreatmentId;

                  _appDbContext.Update(booking);
                  _appDbContext.SaveChanges();
        }
        //..................................................
        //Delete Booking
        public void DeleteBooking(int bookingsId)
        {
            var booking = GetBookingsById(bookingsId);
            // _appDbContext.Remove(booking);
               booking.IsDeleted = true;
            _appDbContext.SaveChanges();
            
        }
        //..................................................
        //Get Booking By Name And Mobile
        public IEnumerable<Bookings> GetBookingsByNameMobile(string searchString, string searchNum)
        {
            var bookings = _appDbContext.Bookingss;
            IList<Bookings> list = new List<Bookings>();
            var B = from b in bookings
                    where (b.Name.Contains(searchString) && b.Mobile.Contains(searchNum)) && b.IsDeleted == false
                    select b;


            foreach (Bookings book in B)
            {
                list.Add(book);
            }
            return list;
        }
    }
}
//Get Booking By Name
//public Bookings GetBookingsByName(string searchString)
//{
//    var bookings = from b in _appDbContext.Bookingss
//                   select b;

//    if (!String.IsNullOrEmpty(searchString))
//    {
//        bookings = bookings.Where(b => b.Name == searchString);
//    }


//    return _appDbContext.Bookingss.FirstOrDefault(b => b.Name == searchString);
//}


////Get All Bookings
//public IEnumerable<Bookings> GetAllBookings()
//{
//    return _appDbContext.Bookingss;
//}
