﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
    public static class DbInitializer
    {
        public static void Seed(IApplicationBuilder applicationBuilder)
        {
           
            using (var scope = applicationBuilder.ApplicationServices.CreateScope())
            {
               AppDbContext context = scope.ServiceProvider.GetService<AppDbContext>();
               
            
            if (!context.Treatments.Any())
            {
                context.AddRange
                    (
                    new LU_Treatment {  TreatmentTitle = "treatment 1", TreatmentDescription = "treatment 1" },
                    new LU_Treatment {  TreatmentTitle = "treatment 2", TreatmentDescription = "treatment 2" },
                    new LU_Treatment { TreatmentTitle = "treatment 3", TreatmentDescription = "treatment 3" }
                    );
                context.SaveChanges();
            }

            if (!context.TimeSlots.Any())
            {
                context.AddRange
                    (
                    new LU_TimeSlot {  TimeSlotTitle= 9  },
                    new LU_TimeSlot {  TimeSlotTitle= 10 },
                    new LU_TimeSlot {  TimeSlotTitle= 11 }
                    );
                context.SaveChanges();
            }
        }
    }
}
}

