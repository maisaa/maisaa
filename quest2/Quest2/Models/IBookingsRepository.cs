﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
   public interface IBookingsRepository
    {
       
        void AddBookings(Bookings bookings);

        IEnumerable<Bookings> GetAllBookings();

        Bookings GetBookingsById(int bookingsId);

        IEnumerable<Bookings> GetBookingsByName(string searchString);

        IEnumerable<Bookings> GetBookingsByNumber(string searchNum);

        IEnumerable<Bookings> GetBookingsByNameMobile(string searchString, string searchNum);

        void DeleteBooking(int bookingsId);

        void UpdateBooking(int bookingsId,Bookings bookings);
    }
}
