using Quest2.Models;
using Xunit;

namespace XUnitTestProject1.Tests
{
    public class Quest2AppEvaluatorShould
    {
        //[Trait("Bookings","LU_TimeSlot")]
        [Fact]
        public void BookingsTest()
        {
            Bookings b = new Bookings();
            Bookings b2 = new Bookings();

            b.Name = "Maisaa";
            b.Mobile = "78786543";
            b.Note = "hiiiiiiiiiiii";
            b.Email = "maisa@g.com";

            Assert.Equal("Maisaa", b.Name);
            Assert.Equal("78786543", b.Mobile);
            Assert.NotEqual("", b.Note);
            Assert.Equal("maisa@g.com", b.Email);
            Assert.False(b.IsDeleted);
            Assert.IsType<Bookings>(b);
            Assert.NotSame(b, b2);
        }
        [Fact]
        public void LU_TimeSlotTest()
        {
            LU_TimeSlot slot = new LU_TimeSlot();

            slot.TimeSlotTitle = 8;

            Assert.Equal(8, slot.TimeSlotTitle);
        }
        [Fact]
        public void Lu_TreatmentTest()
        {
            LU_Treatment treatment = new LU_Treatment();

            treatment.TreatmentTitle = "treatment 1";

            Assert.Equal("treatment 1", treatment.TreatmentTitle);
        }
    }
}
